## How to Install Russian Language Pack

### ✓ Method #1. Composer method (Recommend)
Install the Russian language pack via composer is never easier.

**Install Russian pack**:

```
composer config repositories.language-ru-ru vcs http://git.winmile.top:7990/scm/magento2/language-ru_ru.git
composer require winmile/language-ru_ru:dev-master
php bin/magento setup:static-content:deploy ru_RU
php bin/magento cache:flush

```


**Update Russian pack**:

```
composer config repositories.language-ru-ru vcs http://git.winmile.top:7990/scm/magento2/language-ru_ru.git
composer update winmile/language-ru_ru:dev-master
php bin/magento setup:static-content:deploy ru_RU
php bin/magento cache:flush

```
